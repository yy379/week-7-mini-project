use qdrant_client::{Client, models::{CollectionCreate, FieldSchema, FieldType, VectorParams, Distance}};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
struct MyPayload {
    category: String,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Initialize the client
    let client = Client::new("http://localhost:6333".to_string());

    // Define a new collection
    let collection = CollectionCreate {
        name: "my_collection".to_string(),
        vector_size: 4,
        distance: Distance::Cosine,
        vector_params: VectorParams::default(),
        fields: vec![
            FieldSchema {
                name: "category".to_string(),
                field_type: FieldType::Keyword,
                indexed: true,
            },
        ],
    };

    // Create the collection
    client.create_collection(collection).await?;

    // Example data
    let vectors = vec![
        vec![0.1, 0.2, 0.3, 0.4],
        vec![0.4, 0.3, 0.2, 0.1],
    ];
    let payloads = vec![
        MyPayload { category: "A".to_string() },
        MyPayload { category: "B".to_string() },
    ];

    // Insert data into the collection
    for (i, vector) in vectors.into_iter().enumerate() {
        let payload = &payloads[i];
        client.upsert_point("my_collection", i as i64, &vector, payload).await?;
    }

    println!("Data insertion complete.");
    Ok(())
}
