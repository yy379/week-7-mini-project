# Week7 Cloud Computing Foundations-DevOps

## Requirements
- Ingest data into Vector database
- Perform queries and aggregations
- Visualize output

## Steps to run the project
### Use Cargo to init the project
```bash
cargo init
```

### Add components to main.rs, Cargo.toml, and other files
- Insert vector data in main.rs
```rust
 // Example data
    let vectors = vec![
        vec![0.1, 0.2, 0.3, 0.4],
        vec![0.4, 0.3, 0.2, 0.1],
    ];
    let payloads = vec![
        MyPayload { category: "A".to_string() },
        MyPayload { category: "B".to_string() },
    ];

    // Insert data into the collection
    for (i, vector) in vectors.into_iter().enumerate() {
        let payload = &payloads[i];
        client.upsert_point("my_collection", i as i64, &vector, payload).await?;
    }
```

### Setup Qdrant
```bash
docker pull qdrant/qdrant
docker run -p 6333:6333 qdrant/qdrant
```
![Qdrant](screenshots/qdrant.png)

### Check Qdrant dashboard
- Go to http://localhost:6333/dashboard
![Qdrant Dashboard](screenshots/point.png)
![Qdrant Dashboard](screenshots/result.png)

## References
- [Qdrant](https://qdrant.tech)

## Cargo commands
- `cargo build`: Compiles your project
- `cargo run`: Compiles and runs your project
- `cargo test`: Runs the tests
- `cargo doc`: Builds the documentation
- `cargo update`: Updates your dependencies
- `cargo check`: Checks your code to make sure it compiles but doesn't produce an executable
- `cargo clean`: Removes the target directory

